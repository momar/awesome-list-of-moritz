# Awesome List Of Moritz

## Linux
- **[Tilix](https://github.com/gnunn1/tilix)**: GTK3 terminal emulator with good tiling
- **[Aseprite](https://github.com/aseprite/aseprite)**: pixel art image & animation editor
- **[snaptile](https://github.com/jakebian/snaptile)**: shortcut-based tiling for X11
- **[fusuma](https://github.com/iberianpig/fusuma)**: touchpad gestures for X11
- **[Timetable](https://github.com/lainsce/timetable)**: plot out your own timetable for the week and organize it
- **[Olive](https://github.com/olive-editor/olive)**: an open-source non-linear video editor
- **[Handbrake](https://github.com/HandBrake/HandBrake)**: extremely flexible video converter
- **[catt](https://github.com/skorokithakis/catt)**: send anything to a Chromecast from the command line
- **[CryFS](https://github.com/cryfs/cryfs)**: portable encrypted folders
- **[countdown](https://github.com/antonmedv/countdown)**: command-line countdown
- **[WoeUSB](https://github.com/slacka/WoeUSB)**: create bootable Windows installation USB sticks from Linux
- **[GoTTY](https://github.com/yudai/gotty)**: share command line applications through the browser
- **[Quilter](https://github.com/lainsce/quilter)**: a good markdown notes editor, currently in development
- **[Byte](https://github.com/alainm23/byte)**: a powerful minimalist music player

## Servers, Shell & Docker
- **[Caddy](https://github.com/caddyserver/caddy)**: webserver with focus on simple configuration
- **[fzf](https://github.com/junegunn/fzf)**: fuzzy finder for everything on the the command line
- **[bat](https://github.com/sharkdp/bat)**: `cat` alternative with syntax highlighting, line numbers and more
- **[fd](https://github.com/sharkdp/fd)**: fast & user-friendly `find` alternative
- **[ag/The Silver Searcher](https://github.com/ggreer/the_silver_searcher)**: fast & user-friendly `grep` alternative
- **[sd](https://github.com/chmln/sd)**: user-friendly `sed` alternative
- **[jq](https://stedolan.github.io/jq/)**: JSON query tool
- **[jid](https://github.com/simeji/jid)**: interactive JSON visualisation tool
- **[gron](https://github.com/tomnomnom/gron)**: print JSON in a greppable format
- **[hexyl](https://github.com/sharkdp/hexyl)**: user-friendly viewer for binary files
- **[hecate](https://github.com/evanmiller/hecate)**: user-friendly editor for binary files
- **[transfer.sh](https://github.com/dutchcoders/transfer.sh)**: simple file hosting with shell alias
- **[smell_baron](https://github.com/ohjames/smell-baron)**: tiny init system, awesome for custom Docker images
- **[job](https://github.com/liujianping/job)**: tiny job system for cron-like tasks, awesome for custom Docker images
- **[docker-android](https://github.com/budtmo/docker-android)**: Docker images with Android & NoVNC
- **[dive](https://github.com/wagoodman/dive)**: explore Docker images
- **[ctop](https://github.com/bcicen/ctop)**: manage Docker containers
- **[lazydocker](https://github.com/jesseduffield/lazydocker)**: manage Docker containers and Docker Compose projects
- **[Watchtower](https://github.com/containrrr/watchtower)**: automated Docker container updates
- **[diun](https://github.com/crazy-max/diun)**: Docker image update notifications
- **[slim](https://github.com/ottomatica/slim)**: create micro-VMs from Docker images
- **[LiteCLI](https://github.com/dbcli/litecli)**: user-friendly SQLite command-line client
- **[pgweb](http://sosedoff.github.io/pgweb/)**: PostgreSQL web client
- **[Adminer](https://www.adminer.org/)**: lightweight web-based database management interface; there's also [Peloton](https://github.com/pematon/adminer-theme) which is a nice-looking theme for it
- **[Makisu](https://github.com/uber/makisu)**: unprivileged Docker image build tool
- **[Ansible](https://github.com/ansible/ansible)**: IT automation & deployment platform
- **[Step Certificates](https://github.com/smallstep/certificates)**: local certificate authority
- **[Postal](https://github.com/atech/postal)**: open source mail delivery platform for incoming & outgoing e-mail
- **[dispatch](https://github.com/gesquive/dispatch)**: self-hosted email forwarding microservice
- **[Listmonk](https://github.com/knadh/listmonk)**: self-hosted newsletter and mailing list manager with a modern dashboard
- **[Sonic](https://github.com/valeriansaliou/sonic)**: lightweight Elasticsearch alternative (just a search backend)

## Go
- **[is](https://github.com/matryer/is)**: lightweight testing framework
- **[GoConvey](https://github.com/smartystreets/goconvey)**: tree-based testing framework
- **[Gophers](https://github.com/egonelbre/gophers)**: lots of Gopher pictures undner the CC0 license
- **[Free Gophers Pack](https://github.com/MariaLetta/free-gophers-pack)**: even more Gopher pictures
- **[conf](https://github.com/segmentio/conf)**: very simple configuration loader
- **[configor](https://github.com/jinzhu/configor)**: another configuration loader
- **[go-bindata](https://github.com/go-bindata/go-bindata)**: bundle binary data with your Go application
- **[Ferret](https://github.com/MontFerret/ferret)**: declarative web scraping system
- **[Lorca](https://github.com/zserge/lorca)**: Electron alternative for Go, using the system web-view component
- **[gotk3](https://github.com/gotk3/gotk3)**: GTK3 bindings for Go, which can also be used with [Glade](https://github.com/mrccnt/gotk3-glade-example)
- **[Fyne](https://github.com/fyne-io/fyne)**: Material-inspired UI toolkit
- **[Gorm](https://github.com/jinzhu/gorm)**: ORM library for Go
- **[Starlight](https://github.com/starlight-go/starlight)**: interpreter for the Starlark embedded Python-like language
- **[Tengo](https://github.com/d5/tengo)**: interpreter for an embedded Go-like language
- **[cel](https://github.com/google/cel-go)**: interpreter for another embedded logic language
- **[expr](https://github.com/antonmedv/expr)**: interpreter for an embedded logic language
- **[ObjectBox](https://github.com/objectbox/objectbox-go)**: lightweight object store
- **[glauth](https://github.com/glauth/glauth)**: experimental Go-based LDAP server
- **[go-echarts](https://github.com/chenjiandongx/go-echarts)**: HTML chart generation
- **[Mercure](https://github.com/dunglas/mercure)**: server-sent live updates
- **[cli](https://github.com/kivy/kivy)**: simple command-line applications in Go
- **[Vugu](https://github.com/vugu/vugu)**: WebAssembly & Vue.js based UI library inspired by Vue.js
- **[REST Layer](https://github.com/rs/rest-layer)**: framework for REST APIs
- **[uitable](https://github.com/gosuri/uitable)**: print tables to the terminal
- **[unioffice](https://github.com/unidoc/unioffice)**: create and process Microsoft Office files
- **[unipdf](https://github.com/unidoc/unipdf)**: create and process PDF files
- **[gocui](https://github.com/jroimartin/gocui)**: minimalist Go package aimed at creating Console User Interfaces
- **[swag](https://github.com/swaggo/swag)**: generate Swagger documentation from code
- **[pongo2](https://github.com/flosch/pongo2)**: a more usable templating engine
- **[saml](https://github.com/crewjam/saml)**: SAML/SSO authentication library
- **[script](https://github.com/bitfield/script)**: toolkit for shell-script-like operations
- **[goreadability](https://github.com/philipjkim/goreadability)**: extracts the primary readable content of a webpage
- **[Squirrel](https://github.com/Masterminds/squirrel)**: fluent SQL generation
- **[Logrus](https://github.com/sirupsen/logrus)**: extensible logging library
- **[Ripzap](https://github.com/z0mbie42/rz-go)**: structured & leveled JSON logger
- **[GoHumanize](https://github.com/dustin/go-humanize)**: just a few functions for helping humanize times and sizes
- **[UIProgress](https://github.com/gosuri/uiprogress)**: library to render progress bars in terminal applications
- **[Base64captcha](https://github.com/mojocn/base64Captcha)**: generate & verify captchas
- **[Bitcask](https://github.com/prologic/bitcask)**: simple key-value store

## Javascript, Web & node.js
- **[Vue.js](https://github.com/vuejs/vue)**: lightweight JavaScript web application framework
- **[Trix Editor](https://github.com/basecamp/trix)**: easy-to-use rich text editor
- **[Slate](https://github.com/ianstormtaylor/slate)**: extensible rich text editor
- **[Editor.js](https://github.com/codex-team/editor.js)**: block-based rich text editor
- **[tiptap](https://github.com/scrumpy/tiptap)**: Vue.js rich text editor
- **[gridstack.js](https://github.com/gridstack/gridstack.js)**: grid-based dashboard layouts with drag & drop
- **[Golden Layout](https://github.com/golden-layout/golden-layout)**: widget-based dashboard layout
- **[GoJS](https://github.com/NorthwoodsSoftware/GoJS)**: visualisation of everything that is somehow related to graphs or trees
- **[forgJS](https://github.com/oussamahamdaoui/forgJs)**: object validation framework
- **[Indigo Player](https://github.com/matvp91/indigo-player)**: extensible, modern HTML5 video player with Dash/HLS support
- **[swc](https://github.com/swc-project/swc)**: extremely fast JavaScript transpiler
- **[Framework7](https://github.com/framework7io/framework7)**: mobile HTML framework for Android/iOS apps
- **[water.css](https://github.com/kognise/water.css)**: a minimalist CSS framework
- **[Bulma](https://github.com/jgthms/bulma)**: an awesome CSS framework
- **[Laxxx](https://github.com/alexfoxy/laxxx)**: vanilla javascript plugin to create smooth & beautiful animations when you scroll
- **[dequal](https://github.com/lukeed/dequal)**: simple deep-equal utility
- **[VexChords](https://github.com/0xfe/vexchords)**: guitar chord renderer
- **[Commento](https://github.com/adtac/commento)**: self-hosted comment system
- **[Isso](https://github.com/posativ/isso)**: another self-hosted comment system
- **[Pickr](https://github.com/Simonwep/pickr)**: Color picker
- **[zdog](https://github.com/metafizzy/zdog)**: pseudo-3D engine for flat illustrations
- **[filepond](https://github.com/pqina/filepond)**: file upload library
- **[jExcel](https://github.com/paulhodel/jexcel)**: javascript plugin to create amazing web-based interactive tables and spreadsheets compatible with Excel
- **[Grid Studio](https://github.com/ricklamers/gridstudio)**: web-based spreadsheet application with full integration of the Python programming language
- **[Frontend Checklist](https://github.com/thedaviddias/Front-End-Checklist)**: checklist for modern websites and meticulous developers
- **[Chart.xkcd](https://github.com/timqian/chart.xkcd)**: XKCD-style cartoon chart library

## Development Tools
- **[VS Code](https://github.com/microsoft/vscode)**: awesome open source IDE; binaries built from the open source repository are available at [vscodium.com](https://vscodium.com/); can also be used remotely from the browser using [code-server](https://github.com/cdr/code-server), or directly through SSH using [sshcode]([sshcode](https://github.com/cdr/sshcode))
- **[Hotel](https://github.com/typicode/hotel)**: centrally manage local webservers, with aliases & SSL
- **[ColourPicker](https://github.com/stuartlangridge/ColourPicker)**: Linux application to pick any color from the screen
- **[Peek](https://github.com/phw/peek)**: Linux application to record any area from the screen as a GIF
- **[termtosvg](https://github.com/nbedos/termtosvg)**: record terminal sessions to animated SVGs
- **[Carbon](https://github.com/dawnlabs/carbon)**: create beautiful PNGs from code
- **[Revery](https://www.outrunlabs.com/revery/)**: framework for native, high-performance, cross-platform desktop apps, using [Reason](https://reasonml.github.io/)
- **[Kivy](https://github.com/kivy/kivy)**: framework for Python-based cross-platform UIs
- **[iSH](https://github.com/tbodt/ish)**: Linux shell for iOS
- **[Terminus](https://github.com/Eugeny/terminus)**: Electron-based terminal with integrated SSH client
- **[entr](https://github.com/eradman/entr)**: a utility for running arbitrary commands when files change
- **[codecrumbs](https://github.com/Bogdan-Lyashenko/codecrumbs)**: Breadcrumbs to improve codebase understanding
- **[Insomnia](https://github.com/getinsomnia/insomnia)**: a REST toolkit - if you're looking for something lightweight, there's also [RESTer](https://addons.mozilla.org/en-US/firefox/addon/rester/)
- **[Akira](https://github.com/akiraux/Akira)**: open source UI/UX design application
- **[Web Font Generator](https://github.com/bdusell/webfont-generator)**: locally convert fonts to web formats and generate CSS rules

## Web Applications
- **[dispatch](https://github.com/khlieng/dispatch)**: web IRC client built with Go
- **[fathom](https://github.com/usefathom/fathom)**: privacy-friendly web analytics
- **[Vynchronize](https://github.com/kyle8998/Vynchronize)**: synchronized Youtube playback over the internet
- **[statping](https://github.com/hunterlong/statping)**: monitoring solution for HTTP availability monitoring
- **[bitwarden_rs](https://github.com/dani-garcia/bitwarden_rs)**: a faster & lightweight implementation of the [Bitwarden](https://bitwarden.com/) server
- **[Outline](https://github.com/outline/outline)**: a team wiki, integrated into Slack
- **[Wiki.js](https://github.com/Requarks/wiki)**: a good-looking & powerful wiki application built on node.js
- **[Decidim](https://decidim.org/)**: free open source participatory democracy for cities and organizations
- **[Gotify](https://github.com/gotify/server)**: push notification server with Android app
- **[Gotenberg](https://github.com/thecodingmachine/gotenberg)**: API to convert HTML, Markdown and Office documents to PDF
- **[Jellyfin](https://github.com/jellyfin/jellyfin)**: open source home media server
- **[Filebrowser](https://github.com/filebrowser/filebrowser)**: web-based file manager
- **[Riot.im](https://github.com/vector-im/riot-web)**: decentralized chat client for [Matrix](https://github.com/vector-im/riot-web), you can host your own server using [Synapse](https://github.com/vector-im/riot-web)
- **[Instant.io](https://github.com/webtorrent/instant.io)**: WebTorrent client for simple P2P file sharing
- **[Firefox Send](https://github.com/mozilla/send)**: file sharing software; there's also a [cli client](https://github.com/timvisee/ffsend)
- **[Traduora](https://github.com/traduora/traduora)**: translation management platform for teams
- **[Huginn](https://github.com/huginn/huginn)**: IFTTT-like automated tasks
- **[imgsquash](https://github.com/eashish93/imgsquash)**: Image compression tool
- **[PhotoPrism](https://github.com/photoprism/photoprism)**: Personal Photo Management powered by Go and Google TensorFlow

## Hardware
- **[McLighting](https://github.com/toblum/McLighting)**: ESP8266-based lighting controller
- **[OTTO](https://github.com/topisani/OTTO)**: Sampler, Sequencer, Multi-engine synth and effects
- **[MagicMirror](https://github.com/MichMich/MagicMirror)**: an open source modular smart mirror platform
- **[Labrador](https://github.com/EspoTek/Labrador)**: USB device that transforms your PC or smartphone into a fully-featured electronics lab

## Administrative Tools & Other Lists
- **[Awesome-CV](https://github.com/posquit0/Awesome-CV)**: LaTeX template for CVs
- **[eagle.js](https://github.com/Zulko/eagle.js)**: Vue.js-based slideshow framework
- **[Mermaid](https://github.com/knsv/mermaid)**: Markdown-style diagram generator
- **[Hercule](https://github.com/jamesramsay/hercule)**: Markdown/API Blueprint transclusion/import tool
- **[Snowboard](https://github.com/bukalapak/snowboard)**: beautiful API documentation generator from API Blueprint
- **[Free For Dev](https://github.com/ripienaar/free-for-dev)**: list of XaaS services which are free for development
- **[AgileLite](https://github.com/davebs/AgileLite)**: agile software development without all the burnout
- **[Gophish](https://getgophish.com/)**: Phishing tool

## Everything else
- **[Open62541](https://github.com/open62541/open62541)**: OPC UA library for C
- **[Pizza Dough](https://github.com/hendricius/pizza-dough)**: a recipe for pizza dough
- **[Scoreboard](https://github.com/vatbub/Scoreboard)**: a tool to keep track of scores during card games
- **[scrcpy](https://github.com/Genymobile/scrcpy)**: control Android devices from your PC
- **[RemixIcon](https://github.com/Remix-Design/RemixIcon)**: a fully open-source icon font
- **[macOS Simple KVM](https://github.com/foxlet/macOS-Simple-KVM)**: QEMU-based macOS VM setup
- **[Crystal](https://github.com/crystal-lang/crystal)**: a programming language that's like Ruby, just better
